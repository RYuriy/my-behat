Feature: Test Behat work
  In order to check how framework works

  Scenario: See homepage
    Given I am on "/"
    Then I should see "Каталог товаров"
    When I click on the element with css selector ".f-menu-l-i-link:first-child"
    Then I should be on "/computers-notebooks/c80253/"
