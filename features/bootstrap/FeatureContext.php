<?php

use Behat\Behat\Context\Context;
use Behat\Behat\Tester\Exception\PendingException;
use Behat\Gherkin\Node\PyStringNode;
use Behat\Gherkin\Node\TableNode;
use Behat\MinkExtension\Context\MinkContext;

/**
 * Defines application features from the specific context.
 */
class FeatureContext extends MinkContext implements Context
{

	/**
	 * Initializes context.
	 *
	 * Every scenario gets its own context instance.
	 * You can also pass arbitrary arguments to the
	 * context constructor through behat.yml.
	 */
	public function __construct() {}

	/**
	 * @Then /^I wait "(?P<seconds>\d+)" seconds$/
	 */
	public function iWaitFewSeconds($seconds)
	{
		$this->getSession()->wait($seconds * 1000);
	}

	/**
	 * @When /^I click on the element with css selector "([^"]*)"$/
	 */
	public function iClickButtonByCssSelector($selector)
	{
		$session = $this->getSession();
		$buttonElement = $this->getSession()->getPage()->find(
			'xpath',
			$session->getSelectorsHandler()->selectorToXpath('css', $selector)
		);
		if (!$buttonElement) {
			throw new \InvalidArgumentException("No buttons with selector $selector found in DOM");
		}
		$buttonElement->click();
	}


}//FeatureContext
